var timer;

$(document).ready(function() {
  $('#content').html(content);
  $('.timer').on('click', function(){
    if($(this).html() == '00:00')
    {
      $('.timer').hide();
      $('.set-timer').show();
    }
  });
  
  $('.set-timer').on('click', function(){
    setTimer(parseInt(event.target.innerHTML));
    $('.timer').show();
    $('.set-timer').hide();
  });

  $('.score-card').on('click', function(){
    var $this = $(this);
    id = $this.attr('id');
    value = $this.html()
    value = parseInt(value) + 1;
    $this.html(formatDigit(value, 2));
    var teamName = $('#'+id+'name').html()
    var content = "<tr><td class='"+ id + "name'>" + teamName + "</td><td>" + $('.timer').html() + "</td><td>" + $this.html() + "</td></tr>";
    $('.results').html( $('.results').html() + content );
  });

  $('#teamAname').on('input', changeNameInResults);
  $('#teamBname').on('input', changeNameInResults);
  
  $('#away').on('click', function(){
    var $this = $(this);
    switch($this.attr('state')){
      case 'start':
        debugger;
        var time = $('.timer').html();
        var totalSeconds = timeToSeconds(time);
        if(parseInt(totalSeconds) != 0){
          startTimer();
          $this.attr('state', 'pause');
          $this.html("PAUSE");
        } 
        break;
      case 'pause':
        stopTimer();
        $this.attr('state', 'resume');
        $this.html("RESUME");
        break;
      case 'resume':
        startTimer();
        $this.attr('state', 'pause');
        $this.html("PAUSE");
    }
    $this.addClass('glow');
    $('#home').removeClass('glow');
  });

  $('#home').on('click', function(){
    clearInterval(timer);
    $('.timer').html('00:00');
    $('#away').attr('state', 'start');
    $('#away').html('START');
    $(this).addClass('glow');
    $('#away').removeClass('glow');
  });

  $('.reset').on('click', function(){
    clearInterval(timer);
    $('.timer').html('00:00');
    $('#away').attr('state', 'start');
    $('#away').html('START');
    $('.results').html('');
    $('.score-card').html('00');
    $('#home').removeClass('glow');
    $('#away').removeClass('glow');
  });

});

var secondsToTime = function(totalSeconds){
  var minutes = parseInt(totalSeconds/60)
  var seconds = parseInt(totalSeconds%60);
  minutes = formatDigit(minutes, 2);
  seconds = formatDigit(seconds, 2);
  return minutes + ':' + seconds; 
}

var timeToSeconds = function(time){
  time = time.split(':');
  return time[0] * 60 + parseInt(time[1]);
}

function formatDigit(value, digits){
  preceedingZeros = digits - value.toLocaleString().length
  value = (preceedingZeros > 0) ? "0".repeat(preceedingZeros) + value : value.toLocaleString()
  return value;
}

function changeNameInResults(){
  var $this = $(this);
  id = $this.attr('id');
  $('td.'+id).html($this.html());
}

function setTimer(value){
  var seconds = value * 60;
  var time = secondsToTime(seconds);
  $('.timer').html(time);
}

function startTimer(){
  timer = setInterval(function(){ 
    var time = $('.timer').html();
    var totalSeconds = timeToSeconds(time);
    totalSeconds--;
    time = secondsToTime(totalSeconds);
    $('.timer').html(time);
    if(totalSeconds == 0){
      clearInterval(timer);
      $('#away').attr('state', 'start');
      $('#away').html('START');
    }
  }, 1000);
}

function stopTimer(){
  clearInterval(timer);
}

var content = `<div class="container mt-5 p-5 bg-score text-center text-light">
  <div class="row shadow-lg p-3 team-name">
    <div class="col-sm-5" id="teamAname" contenteditable="true">Team A</div>
    <div class="col-sm-2"></div>
    <div class="col-sm-5" id="teamBname" contenteditable="true">Team B</div>
  </div>
  <div class="row shadow-lg p-3 mt-1">
    <div class="col-sm-5 ">
      <div id="teamA" class="container shadow-lg jumbotron score-card">
        00
      </div>
    </div>
    <div class="col-sm-2">
      <button id="centered-btn" type="button" class="btn btn-outline-light reset">Reset</button>
    </div>
    <div class="col-sm-5">
      <div id="teamB" class="container shadow-lg jumbotron score-card">
        00
      </div>
    </div>
  </div>
  <div class="row shadow-lg p-3 mt-1">
    <div class="col-sm-5 ">
      <button id="home" class="btn btn-outline-light">HOME</button>
    </div>
    <div class="col-sm-2">
      <div class="timer shadow-lg">00:00</div>
      <div class="row set-timer shadow-lg" style="display: none;">
        <div class="col-sm-4">
          5
        </div>
        <div class="col-sm-4 border border-top-0 border-bottom-0">
          10
        </div>
        <div class="col-sm-4">
          15
        </div>
      </div>
    </div>
    <div class="col-sm-5">
      <button id="away" class="btn btn-outline-light" state="start">START</button>
    </div>
  </div>
  <div>
    
  </div>
</div>
<div class="container mt-5 p-0 border shadow-lg text-center">
  <table class="table table-striped">
    <thead class="bg-score text-light">
      <tr>
        <th>Team</th>
        <th>Time</th>
        <th>Score</th>
      </tr>
    </thead>
    <tbody class="results">
    </tbody>
  </table>
</div>`;